EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:3pdt
LIBS:BassCompressor-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1950 3800 1050 800 
U 5B1A3CBC
F0 "Bass_Compressor_Input_Buffer_Bandpass_Splitter" 60
F1 "BC_v1_inp_BandSplitter.sch" 60
F2 "GuitarInput" I L 1950 4200 60 
F3 "InHigh" I R 3000 4000 60 
F4 "InLow" I R 3000 4400 60 
$EndSheet
$Sheet
S 3850 3700 1100 400 
U 5B1D83BE
F0 "BassCOmpressor High Frequency Compression" 60
F1 "BC_v1_HiFreq_comp.sch" 60
F2 "Hi_in" I L 3850 3900 60 
F3 "Hi_out" I R 4950 3900 60 
$EndSheet
$Sheet
S 3850 4400 1100 400 
U 5B1D83EE
F0 "BassCompressor Low Frequency Compression" 60
F1 "BC_v1_LoFreq_comp.sch" 60
F2 "Lo_in" I L 3850 4600 60 
F3 "Lo_out" I R 4950 4600 60 
$EndSheet
$Sheet
S 6850 3950 950  700 
U 5B200E17
F0 "Sum and output buffered stage" 60
F1 "Output_sum.sch" 60
F2 "Hi_in" I L 6850 4150 60 
F3 "Lo_in" I L 6850 4450 60 
F4 "Out_ToAmp" I R 7800 4300 60 
$EndSheet
$Comp
L Audio-Jack-2 J2
U 1 1 5B203286
P 1500 4200
F 0 "J2" H 1475 4275 50  0000 C CNN
F 1 "GuitarInput" H 1475 4025 50  0000 C CNN
F 2 "Wire_Connections_Bridges:WireConnection_1.00mmDrill" H 1750 4200 50  0001 C CNN
F 3 "" H 1750 4200 50  0001 C CNN
	1    1500 4200
	1    0    0    -1  
$EndComp
$Comp
L Audio-Jack-2 J3
U 1 1 5B2037FC
P 8500 4300
F 0 "J3" H 8475 4375 50  0000 C CNN
F 1 "OutputToAmp" H 8475 4125 50  0000 C CNN
F 2 "Wire_Connections_Bridges:WireConnection_1.00mmDrill" H 8750 4300 50  0001 C CNN
F 3 "" H 8750 4300 50  0001 C CNN
	1    8500 4300
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5B203AB8
P 8300 4550
F 0 "#PWR01" H 8300 4300 50  0001 C CNN
F 1 "GND" H 8300 4400 50  0000 C CNN
F 2 "" H 8300 4550 50  0001 C CNN
F 3 "" H 8300 4550 50  0001 C CNN
	1    8300 4550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5B203E69
P 1700 4500
F 0 "#PWR02" H 1700 4250 50  0001 C CNN
F 1 "GND" H 1700 4350 50  0000 C CNN
F 2 "" H 1700 4500 50  0001 C CNN
F 3 "" H 1700 4500 50  0001 C CNN
	1    1700 4500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5B205413
P 2100 2300
F 0 "#PWR03" H 2100 2050 50  0001 C CNN
F 1 "GND" H 2100 2150 50  0000 C CNN
F 2 "" H 2100 2300 50  0001 C CNN
F 3 "" H 2100 2300 50  0001 C CNN
	1    2100 2300
	1    0    0    -1  
$EndComp
$Comp
L D D1
U 1 1 5B2056C7
P 2400 2000
F 0 "D1" H 2400 2100 50  0000 C CNN
F 1 "1N4007" H 2400 1900 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2400 2000 50  0001 C CNN
F 3 "" H 2400 2000 50  0001 C CNN
	1    2400 2000
	0    1    1    0   
$EndComp
$Comp
L CP C28
U 1 1 5B20577A
P 2700 2000
F 0 "C28" H 2725 2100 50  0000 L CNN
F 1 "47u" H 2725 1900 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_6.3x5.3" H 2738 1850 50  0001 C CNN
F 3 "" H 2700 2000 50  0001 C CNN
	1    2700 2000
	1    0    0    -1  
$EndComp
$Comp
L C C29
U 1 1 5B2057F9
P 3000 2000
F 0 "C29" H 3025 2100 50  0000 L CNN
F 1 "100n" H 3025 1900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3038 1850 50  0001 C CNN
F 3 "" H 3000 2000 50  0001 C CNN
	1    3000 2000
	1    0    0    -1  
$EndComp
Text GLabel 2300 1700 1    60   Input ~ 0
Vcc
$Comp
L POT R56
U 1 1 5B2062C1
P 5050 1750
F 0 "R56" V 4875 1750 50  0000 C CNN
F 1 "50k" V 4950 1750 50  0000 C CNN
F 2 "SR_PASSIVES_potentiometer:Potentiometer_SMD_SR_PASSIVES" H 5050 1750 50  0001 C CNN
F 3 "" H 5050 1750 50  0001 C CNN
	1    5050 1750
	1    0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 5B206360
P 5700 2150
F 0 "R19" V 5780 2150 50  0000 C CNN
F 1 "1k" V 5700 2150 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 5630 2150 50  0001 C CNN
F 3 "" H 5700 2150 50  0001 C CNN
	1    5700 2150
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 5B2063F5
P 5300 2050
F 0 "C8" H 5325 2150 50  0000 L CNN
F 1 "100n" H 5325 1950 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 5338 1900 50  0001 C CNN
F 3 "" H 5300 2050 50  0001 C CNN
	1    5300 2050
	1    0    0    -1  
$EndComp
$Comp
L Q_NPN_BCE Q18
U 1 1 5B20645E
P 5600 1750
F 0 "Q18" H 5800 1800 50  0000 L CNN
F 1 "BC547C" H 5800 1700 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 5800 1850 50  0001 C CNN
F 3 "" H 5600 1750 50  0001 C CNN
	1    5600 1750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5B206899
P 5700 2450
F 0 "#PWR04" H 5700 2200 50  0001 C CNN
F 1 "GND" H 5700 2300 50  0000 C CNN
F 2 "" H 5700 2450 50  0001 C CNN
F 3 "" H 5700 2450 50  0001 C CNN
	1    5700 2450
	1    0    0    -1  
$EndComp
Text GLabel 5400 1400 1    60   Input ~ 0
Vcc
Text GLabel 5950 2000 2    60   Input ~ 0
vbias
$Comp
L +9V #PWR05
U 1 1 5B214805
P 1600 1850
F 0 "#PWR05" H 1600 1700 50  0001 C CNN
F 1 "+9V" H 1600 1990 50  0000 C CNN
F 2 "" H 1600 1850 50  0001 C CNN
F 3 "" H 1600 1850 50  0001 C CNN
	1    1600 1850
	0    1    1    0   
$EndComp
$Comp
L PWR_FLAG #FLG06
U 1 1 5B21D175
P 1600 2300
F 0 "#FLG06" H 1600 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 1600 2450 50  0000 C CNN
F 2 "" H 1600 2300 50  0001 C CNN
F 3 "" H 1600 2300 50  0001 C CNN
	1    1600 2300
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG07
U 1 1 5B21D39F
P 1600 1700
F 0 "#FLG07" H 1600 1775 50  0001 C CNN
F 1 "PWR_FLAG" H 1600 1850 50  0000 C CNN
F 2 "" H 1600 1700 50  0001 C CNN
F 3 "" H 1600 1700 50  0001 C CNN
	1    1600 1700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02_Male J1
U 1 1 5B26BB31
P 1400 1900
F 0 "J1" H 1400 2000 50  0000 C CNN
F 1 "+9V power" H 1400 1700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch1.00mm" H 1400 1900 50  0001 C CNN
F 3 "" H 1400 1900 50  0001 C CNN
	1    1400 1900
	1    0    0    -1  
$EndComp
Connection ~ 1600 1850
Wire Wire Line
	7800 4300 8300 4300
Wire Wire Line
	1700 4200 1950 4200
Wire Wire Line
	3450 4400 3000 4400
Wire Wire Line
	3450 4600 3450 4400
Wire Wire Line
	3850 4600 3450 4600
Wire Wire Line
	3450 3900 3850 3900
Wire Wire Line
	3450 4000 3450 3900
Wire Wire Line
	3000 4000 3450 4000
Wire Wire Line
	1600 1700 3000 1700
Wire Wire Line
	1600 2000 1600 2300
Connection ~ 5300 1750
Wire Wire Line
	5300 1900 5300 1750
Connection ~ 5300 2400
Wire Wire Line
	5300 2200 5300 2400
Wire Wire Line
	5700 1400 5700 1550
Wire Wire Line
	5050 1600 5050 1400
Wire Wire Line
	5050 2400 5050 1900
Connection ~ 5700 2400
Wire Wire Line
	5700 2300 5700 2450
Wire Wire Line
	5050 2400 5700 2400
Wire Wire Line
	5700 2000 5700 1950
Wire Wire Line
	5200 1750 5400 1750
Connection ~ 2400 2300
Wire Wire Line
	2400 2150 2400 2300
Connection ~ 2700 2300
Wire Wire Line
	2700 2150 2700 2300
Connection ~ 2700 1700
Wire Wire Line
	2700 1700 2700 1850
Connection ~ 2400 1700
Wire Wire Line
	2400 1850 2400 1700
Connection ~ 2100 2300
Wire Wire Line
	3000 2300 3000 2150
Wire Wire Line
	3000 1700 3000 1850
Wire Wire Line
	1600 1700 1600 1900
Wire Wire Line
	1600 2300 3000 2300
Wire Wire Line
	1700 4500 1700 4300
Wire Wire Line
	8300 4550 8300 4400
Wire Wire Line
	6500 4600 4950 4600
Wire Wire Line
	6500 4450 6500 4600
Wire Wire Line
	6850 4450 6500 4450
Wire Wire Line
	6500 4150 6850 4150
Wire Wire Line
	6500 3900 6500 4150
Wire Wire Line
	4950 3900 6500 3900
Wire Wire Line
	5050 1400 5700 1400
Wire Wire Line
	5950 2000 5700 2000
$EndSCHEMATC
