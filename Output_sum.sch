EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:3pdt
LIBS:BassCompressor-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1900 2600 0    60   Input ~ 0
Hi_in
Text HLabel 1900 3700 0    60   Input ~ 0
Lo_in
Text HLabel 3800 3850 2    60   Input ~ 0
Out_ToAmp
$Comp
L C C23
U 1 1 5B201F10
P 2100 2950
F 0 "C23" H 2125 3050 50  0000 L CNN
F 1 "100n" H 2125 2850 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2138 2800 50  0001 C CNN
F 3 "" H 2100 2950 50  0001 C CNN
	1    2100 2950
	1    0    0    -1  
$EndComp
$Comp
L POT R53
U 1 1 5B201F88
P 2100 3400
F 0 "R53" V 1925 3400 50  0000 C CNN
F 1 "100k" V 2000 3400 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_WirePads_Small" H 2100 3400 50  0001 C CNN
F 3 "" H 2100 3400 50  0001 C CNN
	1    2100 3400
	1    0    0    -1  
$EndComp
$Comp
L Q_NPN_BCE Q15
U 1 1 5B201FA9
P 2850 3400
F 0 "Q15" H 3050 3450 50  0000 L CNN
F 1 "BC547C" H 3050 3350 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 3050 3500 50  0001 C CNN
F 3 "" H 2850 3400 50  0001 C CNN
	1    2850 3400
	1    0    0    -1  
$EndComp
$Comp
L POT R55
U 1 1 5B202036
P 2950 3850
F 0 "R55" V 2775 3850 50  0000 C CNN
F 1 "10k" V 2850 3850 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_WirePads_Small" H 2950 3850 50  0001 C CNN
F 3 "" H 2950 3850 50  0001 C CNN
	1    2950 3850
	1    0    0    -1  
$EndComp
$Comp
L CP1 C24
U 1 1 5B20207B
P 3400 3850
F 0 "C24" H 3425 3950 50  0000 L CNN
F 1 "22u" H 3425 3750 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_4x5.3" H 3400 3850 50  0001 C CNN
F 3 "" H 3400 3850 50  0001 C CNN
	1    3400 3850
	0    -1   -1   0   
$EndComp
$Comp
L R R54
U 1 1 5B20211C
P 3800 4100
F 0 "R54" V 3880 4100 50  0000 C CNN
F 1 "100k" V 3800 4100 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 3730 4100 50  0001 C CNN
F 3 "" H 3800 4100 50  0001 C CNN
	1    3800 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3100 2100 3250
Wire Wire Line
	2250 3400 2650 3400
Wire Wire Line
	2950 3600 2950 3700
Wire Wire Line
	3100 3850 3250 3850
Wire Wire Line
	3550 3850 3800 3850
Wire Wire Line
	3800 3850 3800 3950
Wire Wire Line
	1900 2600 2100 2600
Wire Wire Line
	2100 2600 2100 2800
Wire Wire Line
	2100 3550 2100 3700
Wire Wire Line
	2100 3700 1900 3700
Wire Wire Line
	2950 4000 2950 4350
Wire Wire Line
	2950 4350 3800 4350
Wire Wire Line
	3800 4350 3800 4250
$Comp
L GND #PWR037
U 1 1 5B202206
P 3400 4400
F 0 "#PWR037" H 3400 4150 50  0001 C CNN
F 1 "GND" H 3400 4250 50  0000 C CNN
F 2 "" H 3400 4400 50  0001 C CNN
F 3 "" H 3400 4400 50  0001 C CNN
	1    3400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 4400 3400 4350
Connection ~ 3400 4350
Wire Wire Line
	2950 3200 2950 2700
Text GLabel 2950 2700 1    60   Input ~ 0
Vcc
$EndSCHEMATC
